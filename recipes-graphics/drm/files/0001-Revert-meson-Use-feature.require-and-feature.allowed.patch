From b45bac9a2dba701c3fdb3176f68b93a5034d05eb Mon Sep 17 00:00:00 2001
From: Marek Vasut <marex@denx.de>
Date: Fri, 22 Dec 2023 01:45:16 +0100
Subject: [PATCH libdrm 1/2] Revert "meson: Use feature.require() and
 feature.allowed()"

This reverts commit 16e6a965051fae4d5142e9f16c47bb6bd3dd6f59.

This is necessary to permit build with old meson version,
which is present in OE dunfell release.

Upstream-Status: Inappropriate [OE dunfell workaround]
Signed-off-by: Marek Vasut <marex@denx.de>
---
 .gitlab-ci.yml |   4 +-
 meson.build    | 120 +++++++++++++++++++++++++++++++++++--------------
 2 files changed, 88 insertions(+), 36 deletions(-)

diff --git a/.gitlab-ci.yml b/.gitlab-ci.yml
index 6e8655fa..b59d68ea 100644
--- a/.gitlab-ci.yml
+++ b/.gitlab-ci.yml
@@ -41,10 +41,10 @@ stages:
     BUILD_OS: debian
     FDO_DISTRIBUTION_VERSION: buster
     FDO_DISTRIBUTION_PACKAGES: 'build-essential docbook-xsl libatomic-ops-dev libcairo2-dev libcunit1-dev libpciaccess-dev meson ninja-build pkg-config python3 python3-pip python3-wheel python3-setuptools python3-docutils valgrind'
-    FDO_DISTRIBUTION_EXEC: 'pip3 install meson==0.59.0'
+    FDO_DISTRIBUTION_EXEC: 'pip3 install meson==0.53.0'
     # bump this tag every time you change something which requires rebuilding the
     # base image
-    FDO_DISTRIBUTION_TAG: "2023-09-01.0"
+    FDO_DISTRIBUTION_TAG: "2022-08-22.0"
 
 .debian-x86_64:
   extends:
diff --git a/meson.build b/meson.build
index c0a7e4ca..61f67114 100644
--- a/meson.build
+++ b/meson.build
@@ -23,7 +23,7 @@ project(
   ['c'],
   version : '2.4.118',
   license : 'MIT',
-  meson_version : '>= 0.59',
+  meson_version : '>= 0.53',
   default_options : ['buildtype=debugoptimized', 'c_std=c11'],
 )
 
@@ -84,61 +84,113 @@ config.set10('HAVE_LIB_ATOMIC_OPS', lib_atomics)
 
 dep_pciaccess = dependency('pciaccess', version : '>= 0.10', required : get_option('intel'))
 
-with_intel = get_option('intel') \
-  .require(with_atomics, error_message : 'libdrm_intel requires atomics') \
-  .require(dep_pciaccess.found(), error_message : 'libdrm_intel requires libpciaccess') \
-  .disable_auto_if(not host_machine.cpu_family().startswith('x86')) \
-  .allowed()
+with_intel = false
+_intel = get_option('intel')
+if not _intel.disabled()
+  if _intel.enabled()
+    if not with_atomics
+      error('libdrm_intel requires atomics.')
+    elif not dep_pciaccess.found()
+      error('libdrm_intel requires libpciaccess')
+    endif
+  else
+    with_intel = (_intel.enabled() or host_machine.cpu_family().startswith('x86')) and with_atomics and dep_pciaccess.found()
+  endif
+endif
 summary('Intel', with_intel)
 
-with_radeon = get_option('radeon') \
-  .require(with_atomics, error_message : 'libdrm_radeon requires atomics') \
-  .allowed()
+with_radeon = false
+_radeon = get_option('radeon')
+if not _radeon.disabled()
+  if _radeon.enabled() and not with_atomics
+    error('libdrm_radeon requires atomics.')
+  endif
+  with_radeon = with_atomics
+endif
 summary('Radeon', with_radeon)
 
-with_amdgpu = get_option('amdgpu') \
-  .require(with_atomics, error_message : 'libdrm_amdgpu requires atomics') \
-  .allowed()
+with_amdgpu = false
+_amdgpu = get_option('amdgpu')
+if not _amdgpu.disabled()
+  if _amdgpu.enabled() and not with_atomics
+    error('libdrm_amdgpu requires atomics.')
+  endif
+  with_amdgpu = with_atomics
+endif
 summary('AMDGPU', with_amdgpu)
 
-with_nouveau = get_option('nouveau') \
-  .require(with_atomics, error_message : 'libdrm_nouveau requires atomics') \
-  .allowed()
+with_nouveau = false
+_nouveau = get_option('nouveau')
+if not _nouveau.disabled()
+  if _nouveau.enabled() and not with_atomics
+    error('libdrm_nouveau requires atomics.')
+  endif
+  with_nouveau = with_atomics
+endif
 summary('Nouveau', with_nouveau)
 
-with_vmwgfx = get_option('vmwgfx').allowed()
+with_vmwgfx = false
+_vmwgfx = get_option('vmwgfx')
+if not _vmwgfx.disabled()
+  with_vmwgfx = true
+endif
 summary('vmwgfx', with_vmwgfx)
 
-with_omap = get_option('omap') \
-  .require(with_atomics, error_message : 'libdrm_omap requires atomics') \
-  .enabled()
+with_omap = false
+_omap = get_option('omap')
+if _omap.enabled()
+  if not with_atomics
+    error('libdrm_omap requires atomics.')
+  endif
+  with_omap = true
+endif
 summary('OMAP', with_omap)
 
-with_freedreno = get_option('freedreno') \
-  .require(with_atomics, error_message : 'libdrm_freedreno requires atomics') \
-  .disable_auto_if(not ['arm', 'aarch64'].contains(host_machine.cpu_family())) \
-  .allowed()
+with_freedreno = false
+_freedreno = get_option('freedreno')
+if not _freedreno.disabled()
+  if _freedreno.enabled() and not with_atomics
+    error('libdrm_freedreno requires atomics.')
+  else
+    with_freedreno = (_freedreno.enabled() or ['arm', 'aarch64'].contains(host_machine.cpu_family())) and with_atomics
+  endif
+endif
 summary('Freedreno', with_freedreno)
 summary('Freedreon-kgsl', with_freedreno_kgsl)
 
-with_tegra = get_option('tegra') \
-  .require(with_atomics, error_message : 'libdrm_tegra requires atomics') \
-  .disable_auto_if(not ['arm', 'aarch64'].contains(host_machine.cpu_family())) \
-  .enabled()
+with_tegra = false
+_tegra = get_option('tegra')
+if _tegra.enabled()
+  if not with_atomics
+    error('libdrm_tegra requires atomics.')
+  endif
+  with_tegra = true
+endif
 summary('Tegra', with_tegra)
 
-with_etnaviv = get_option('etnaviv') \
-  .require(with_atomics, error_message : 'libdrm_etnaviv requires atomics') \
-  .disable_auto_if(not ['arm', 'aarch64', 'arc', 'mips', 'mips64', 'loongarch64'].contains(host_machine.cpu_family())) \
-  .allowed()
+with_etnaviv = false
+_etnaviv = get_option('etnaviv')
+if not _etnaviv.disabled()
+  if _etnaviv.enabled() and not with_atomics
+    error('libdrm_etnaviv requires atomics.')
+  endif
+  with_etnaviv = _etnaviv.enabled() or (
+    with_atomics and [
+      'loongarch64', 'mips', 'mips64',
+      'arm', 'aarch64', 'arc',
+    ].contains(host_machine.cpu_family())
+  )
+endif
 summary('Etnaviv', with_etnaviv)
 
 with_exynos = get_option('exynos').enabled()
 summary('EXYNOS', with_exynos)
 
-with_vc4 = get_option('vc4') \
-  .disable_auto_if(not ['arm', 'aarch64'].contains(host_machine.cpu_family())) \
-  .allowed()
+with_vc4 = false
+_vc4 = get_option('vc4')
+if not _vc4.disabled()
+  with_vc4 = _vc4.enabled() or ['arm', 'aarch64'].contains(host_machine.cpu_family())
+endif
 summary('VC4', with_vc4)
 
 # Among others FreeBSD does not have a separate dl library.
-- 
2.43.0

