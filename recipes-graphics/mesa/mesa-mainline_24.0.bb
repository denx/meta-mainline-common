GALLIUMDRIVERS = "swrast"
require mesa-mainline.inc

DEFAULT_PREFERENCE ?= "${@'1' if (d.getVar('LAYERSERIES_CORENAMES') in ["kirkstone"]) else '-1'}"

SRC_URI = " git://gitlab.freedesktop.org/mesa/mesa.git;branch=24.0;protocol=https "
S = "${WORKDIR}/git"
SRCREV = "312782d55fccb99b6531d6c6e7f6a9a84a2be33a"
PV = "24.0.9"

PACKAGECONFIG[dri3] = "-Ddri3=enabled, -Ddri3=disabled, xorgproto libxshmfence"
PACKAGECONFIG:append = " \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11 opengl', 'dri3', '', d)} \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11 vulkan', 'dri3', '', d)} \
	"

PACKAGECONFIG[kmsro] = ""
GALLIUMDRIVERS:append ="${@bb.utils.contains('PACKAGECONFIG', 'kmsro', ',kmsro', '', d)}"
