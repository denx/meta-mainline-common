GALLIUMDRIVERS = "swrast"
require mesa-mainline.inc

DEFAULT_PREFERENCE ?= "${@'1' if (d.getVar('LAYERSERIES_CORENAMES') in ["dunfell"]) else '-1'}"

SRC_URI = " git://gitlab.freedesktop.org/mesa/mesa.git;branch=23.0;protocol=https "
S = "${WORKDIR}/git"
SRCREV = "896db04e025a2623bda7b9b6225dc565019e5a22"
PV = "23.0.4"

# Since mesa 23.0.y the minimum meson version is 0.54.0 due to ncurses
# pkg-config workarounds now part of this meson version. For details,
# see mesa commit f3a9076e51d43f76e0aee81659d661b5b60c0756 . Since the
# ncurses pkg-config workaround is not applicable to OE builds and OE
# dunfell release still ships only meson 0.53.2, roll back the meson
# version requirement for OE dunfell builds to 0.53 here.
do_configure:prepend() {
	if [ "${LAYERSERIES_CORENAMES}" = "dunfell" ] ; then
		sed -i "/meson_version : '>= 0.54',/ s@0.54@0.53@" ${S}/meson.build
	fi
}

PACKAGECONFIG[dri3] = "-Ddri3=enabled, -Ddri3=disabled, xorgproto libxshmfence"
PACKAGECONFIG:append = " \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11 opengl', 'dri3', '', d)} \
	${@bb.utils.contains('DISTRO_FEATURES', 'x11 vulkan', 'dri3', '', d)} \
	"

PACKAGECONFIG[kmsro] = ""
GALLIUMDRIVERS:append ="${@bb.utils.contains('PACKAGECONFIG', 'kmsro', ',kmsro', '', d)}"
