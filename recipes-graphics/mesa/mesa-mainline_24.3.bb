GALLIUMDRIVERS = "softpipe"
require mesa-mainline.inc

DEFAULT_PREFERENCE = "-1"

SRC_URI = " git://gitlab.freedesktop.org/mesa/mesa.git;branch=24.3;protocol=https "
S = "${WORKDIR}/git"
SRCREV = "1950a8b78caf707663fb30e7b951817e6d3f3297"
PV = "24.3.4"

# License moved into separate directory in 24.3.3
LIC_FILES_CHKSUM = "file://docs/license.rst;md5=ffe678546d4337b732cfd12262e6af11"
