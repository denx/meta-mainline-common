# board specific branches
KBRANCH ?= "linux-6.1.y"
LINUX_VERSION ?= "6.1.130"

SRCREV_machine ?= "6ae7ac5c4251b139da4b672fe4157f2089a9d922"
SRCREV_meta ?= "b292fbe55b1863cdb444fca24634f89b016215ed"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=yocto-6.1;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require linux-stable.inc
