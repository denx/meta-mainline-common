# board specific branches
KBRANCH ?= "linux-6.12.y"
LINUX_VERSION ?= "6.12.18"

SRCREV_machine ?= "105a31925e2d17b766cebcff5d173f469e7b9e52"
SRCREV_meta ?= "a78899d19a4c207b9fc7bf6726e583d0bcfad690"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=master;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require linux-stable.inc
