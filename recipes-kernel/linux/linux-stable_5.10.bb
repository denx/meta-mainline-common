# board specific branches
KBRANCH ?= "linux-5.10.y"
LINUX_VERSION ?= "5.10.234"

SRCREV_machine ?= "f0a53361993a94f602df6f35e78149ad2ac12c89"
SRCREV_meta ?= "dade99e71814568e98f6acbe837284f88a9053f1"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=yocto-5.10;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require linux-stable.inc
