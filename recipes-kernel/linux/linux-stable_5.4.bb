# board specific branches
KBRANCH ?= "linux-5.4.y"
LINUX_VERSION ?= "5.4.290"

SRCREV_machine ?= "856a224845f949243d6719165c88a70e4b473ec4"
SRCREV_meta ?= "ee9020d3cb8056f5142b3ddcc209005fc108a79e"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=yocto-5.4;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

require linux-stable.inc
