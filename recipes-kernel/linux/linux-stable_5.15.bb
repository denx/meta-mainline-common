# board specific branches
KBRANCH ?= "linux-5.15.y"
LINUX_VERSION ?= "5.15.178"

SRCREV_machine ?= "c16c81c81336c0912eb3542194f16215c0a40037"
SRCREV_meta ?= "3ee7eccea9eb7b64f35fe3abe9547cc39fe43dc9"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=yocto-5.15;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require linux-stable.inc
