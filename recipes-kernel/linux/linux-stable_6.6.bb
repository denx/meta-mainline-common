# board specific branches
KBRANCH ?= "linux-6.6.y"
LINUX_VERSION ?= "6.6.81"

SRCREV_machine ?= "640fad833ddce708ec92640ec247d4139433dc3b"
SRCREV_meta ?= "3b089b2e60f6d6daec2e4b1e2f094b01071f4825"

require recipes-kernel/linux/linux-yocto.inc

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git;protocol=https;name=machine;branch=${KBRANCH}; \
           git://git.yoctoproject.org/yocto-kernel-cache;type=kmeta;protocol=https;name=meta;branch=yocto-6.6;destsuffix=${KMETA}"

LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

require linux-stable.inc
