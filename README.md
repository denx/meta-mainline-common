Mainline common OpenEmbedded layer
==================================

This layer provides newer upstream versions of the following base components:
U-Boot, Linux kernel and Mesa.

Dependencies
------------

This layer depends on:

* URI: https://git.openembedded.org/openembedded-core
  - branch: dunfell, kirkstone, or scarthgap

Contributing
------------

Please submit any patches against the `meta-mainline-common` layer
via email to maintainers.

Maintainers
-----------

- Marek Vasut <marex@denx.de>
