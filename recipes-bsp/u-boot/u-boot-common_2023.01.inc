require u-boot-common.inc

LIC_FILES_CHKSUM = "file://Licenses/README;md5=2ca5f2c35c8cc335f0a19756634782f1"

# We use the revision in order to avoid having to fetch it from the
# repo during parse
SRCREV = "62e2ad1ceafbfdf2c44d3dc1b6efc81e768a96b9"
