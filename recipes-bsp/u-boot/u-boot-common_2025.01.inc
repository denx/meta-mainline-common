require u-boot-common.inc

LIC_FILES_CHKSUM = "file://Licenses/README;md5=2ca5f2c35c8cc335f0a19756634782f1"

# We use the revision in order to avoid having to fetch it from the
# repo during parse
SRCREV = "6d41f0a39d6423c8e57e92ebbe9f8c0333a63f72"

DEPENDS += "gnutls-native"
